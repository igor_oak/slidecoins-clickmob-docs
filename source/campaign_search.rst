Algoritmo de busca de campanhas
===============================

Campanha elegível
-----------------

Uma campanha só ficará disponível para consulta se atender os seguintes requisitos:

- Ela precisa ter sido ativada pelo administrador (``is_active=True``).
- Ela precisa ter seu número de visualizações >= 1.
- A data da solicitação precisa estar entre a data inicial e final da campanha.
- Ela não pode estar cancelada (``ìs_canceled=False``). O usuário pode cancelar sua campanha, obtendo de volta o número de visualizações restantes para utilizar, mas nesse caso a campanha não estará disponível para visualização.

Parâmetros
----------

Todos os parâmetros são opcionais.

- lat & lng: Latitude e Longitude de onde a solicitação é enviada
- city: Cidade de onde a solicitação é enviada
- state: Estado de onde a solicitação é enviada
- country: País de onde a solicitação é enviada, default Brasil.
- age: Idade que a campanha é destinada.
- gender: Orientação sexual que a campanha é destinada: M (Masculino), F (Feminino), ou O (Outros)

Exemplo: ``/campaigns/show/?lat=1.2323&lng=24.24242&city=Guarulhos&state=PA&age=18&gender=M``

Precedência de busca de campanhas
---------------------------------

- Primeiro o sistema tenta encontrar campanhas pela localização do usuário (latitude e longitude) desde que uma campanha tenha sido criada com abrangência de centro e raio (mapa). **Prioridade 5**.
- Caso o sistema não encontre campanhas pelo passo 1, ele tenta encontrar campanhas pela cidade informada. **Prioridade 4**.
- Caso o sistema não encontra campanhas pelo passo2, ele tenta encontrar campanhas pelo estado informado. **Prioridade 3**.
- Caso o sistema não encontre campanhas pelo passo3, ele tenta encontrar campanhas pelo país informado (default Brasil). **Prioridade 2**.
- Caso o sistema não encontre nenhuma campanha pelos parâmetros informados, ele retorna uma campanha elegível **aleatória**. **Prioridade 1**.

Se nenhum parâmetro GET (querystring) for enviado, o sistema retorna uma campanha elegível **aleatória**.

Se mais de uma campanha for obtida em uma dada prioridade, o sistema retorna uma campanha **aleatória entre elas**.

Se uma campanha foi criada com uma **idade mínima**, o sistema só vai retornar essa campanha se a idade for informada na consulta. Por exemplo: uma campanha foi criada com idade mínima 18. Se a idade não for informada na busca, ou a idade informada é menor que a idade da campanha, essa campanha **não será visualizada**. Por exemplo: ``/campaings/show/`` ou ``/campaigns/show/?age=17`` não retornariam essa campanha com idade mínima 18 anos.

Se uma campanha foi criada para um gênero específico, o sistema só vai retornar essa campanha se o gênero for informado na consulta. Por exemplo: uma campanha foi criada para o gênero ``M``. Se o gênero não foi informado na busca, ou se o gênero informado é diferente do gênero destinado da campanha, essa campanha **não será visualizada**. Por exemplo: ``/campaings/show/`` ou ``/campaigns/show/?gender=F`` não retornariam essa campanha destinada ao gênero ``M``.
